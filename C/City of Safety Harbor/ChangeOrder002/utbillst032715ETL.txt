 Page 99
             XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
             XXXXXXXXX-XXXXXXXXX XX-XX   99/99/99   99/99/99
                     XXXXXXXXXXXXXXXXXXXXXXXXX  9,999,999.99
                     XXXXXXXXXXXXXXXXXXXXXXXXX  9,999,999.99
                     XXXXXXXXXXXXXXXXXXXXXXXXX  9,999,999.99
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
     XXXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXX
 99999999   XXXXX
                                                              XXXXXXXXXXXXXXXXXX
                  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                                                XXXXXXXXXXXXXXXX  9,999,999.99
 XXXXXXXX-XXXXXXXXXX  XX-XX  99/99/99  99/99/99 XXXXXXXXXXXXXXXX  9,999,999.99
                                                XXXXXXXXXXXXXXXX  9,999,999.99
 XXXXXXXXXXXXX X XXXXXXXXXXXXXXXXXXXX           XXXXXXXXXXXXXXXX  9,999,999.99
           Last payment amount/date: 9,999,999.99  99/99/99
       Service Period Days Meter Number XXXX XXXXX   Current   Previous   Usage
 XX 99/99/99  99/99/99 99  XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 XX 99/99/99  99/99/9999   XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 XX 99/99/99  99/99/9999   XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 XX 99/99/99  99/99/9999   XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 Service                        Consumption            Charge             Total
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
     XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
     XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
                             XXXXXXXXXXXXXXXXXXXXXXXXX             9,999,999.99
                             XXXXXXXXXXXXXXXXXXXXXXXXX             9,999,999.99
                             XXXXXXXXXXXXXXXXXXXXXXXXX             9,999,999.99
                           Pay 9,999,999.99 After 99/99/99
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  Page  1
           212 7TH ST N
           54669-5733          01-02   3/30/15    4/09/15
                  Total Current Charges             28.30-
                  ** Balance Forward **             83.68
                  Total Amount Due                  55.38
        JANICE FITZGERALD
        10705 CENTRAL PARK AVE                     00005466900000573300000005538
        TRINITY FL 34655
        1
                                                              *** Final Bill ***
                  212 7TH ST N
                                                     Last Bill Amount     83.68
  54669-5733          01-02   3/30/15    4/09/15     Payments               .00
                                                     Adjustments            .00
                                                     Balance Forward      83.68
      Service Period  Days Meter Number             Current  Previous     Usage
 WA  3/02/15  3/26/15  24   07063902                   1979      1963        16
 Service                        Consumption            Charge            Total
 SL  STREET LIGHT ASSESS   3/13/15   3/26/15             6.50             6.50
 WA  WATER SERVICE                    16.00             16.78            16.78
 SW  SEWER SERVICE                    16.00             20.60            20.60
 GA  SANITATION SERVICE    3/02/15   3/26/15            17.86            17.86
 ST  STORMWATER SERVICE    3/13/15   3/26/15             7.25             7.25
 RE  RECYCLING SERVICE     3/13/15   3/26/15             2.77             2.77
     DEPOSIT REFUND          3/27/15                                    100.00-
     DEPOSIT INTEREST        3/27/15                                       .06-
                             Total Current Charges                       28.30-
                             ** Balance Forward **                       83.68
                             Total Amount Due                            55.38
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ***************************************************************************
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
  Page  1
           214 7TH ST N
           54671-8132          01-02   3/30/15    4/09/15
                  Total Current Charges             36.24-
                  ** Balance Forward **              2.75-
                  Credit Balance Do Not Pay         38.99-
        AMBER FINK
        10705 CENTRAL PARK AVE                     00005467100000813200000000000
        TRINITY FL 34655
        2
                                                              *** Final Bill ***
                  214 7TH ST N
                                                     Last Bill Amount    122.25
  54671-8132          01-02   3/30/15    4/09/15     Payments            125.00-
                                                     Adjustments            .00
                                                     Balance Forward       2.75-
      Service Period  Days Meter Number             Current  Previous     Usage
 WA  3/02/15  3/26/15  24   07063899                   1938      1924        14
 Service                        Consumption            Charge            Total
 WA  WATER SERVICE                    14.00             16.37            16.37
 SW  SEWER SERVICE                    14.00             19.57            19.57
 GA  SANITATION SERVICE    3/02/15   3/26/15            17.86            17.86
 ST  STORMWATER SERVICE    3/13/15   3/26/15             7.25             7.25
 RE  RECYCLING SERVICE     3/13/15   3/26/15             2.77             2.77
     DEPOSIT REFUND          3/27/15                                    100.00-
     DEPOSIT INTEREST        3/27/15                                       .06-
                             Total Current Charges                       36.24-
                             ** Balance Forward **                        2.75-
                             Credit Balance Do Not Pay                   38.99-
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ***************************************************************************
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
  Page  1
           628 QUAIL KEEP DR
           62787-3839          02-26   3/30/15    4/09/15
                  Total Current Charges             41.83
                  ** Balance Forward **               .00
                  Total Amount Due                  41.83
        OCCUPANT
        628 QUAIL KEEP DR                          00006278700000383900000004183
        SAFETY HARBOR FL 34695
        3
                                                              *** Final Bill ***
                  628 QUAIL KEEP DR
                                                     Last Bill Amount       .00
  62787-3839          02-26   3/30/15    4/09/15     Payments               .00
                                                     Adjustments            .00
                                                     Balance Forward        .00
      Service Period  Days Meter Number             Current  Previous     Usage
 WA  3/18/15  3/20/15   2   8663272                    5881      5881         0
 WA  3/20/15  3/26/15   6   8663273                    5881      5881         0
 Service                        Consumption            Charge            Total
 WA  WATER SERVICE                                      13.53            13.53
 SW  SEWER SERVICE                                      12.33            12.33
 GA  SANITATION SERVICE    3/18/15   3/26/15             5.95             5.95
 ST  STORMWATER SERVICE    3/18/15   3/26/15             7.25             7.25
 RE  RECYCLING SERVICE     3/18/15   3/26/15             2.77             2.77
                             Total Current Charges                       41.83
                             ** Balance Forward **                         .00
                             Total Amount Due                            41.83
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ***************************************************************************
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
  Page  1
           1010 WYNDHAM WAY
           14587-2937          02-27   3/30/15    4/09/15
                  Total Current Charges             28.02-
                  ** Balance Forward **             89.16
                  Total Amount Due                  61.14
        LYDIA RUNNELS
        2295 CAROLYN DRIVE                         00001458700000293700000006114
        DUNEDIN FL 34698
        4
                                                              *** Final Bill ***
                  1010 WYNDHAM WAY
                                                     Last Bill Amount     89.16
  14587-2937          02-27   3/30/15    4/09/15     Payments               .00
                                                     Adjustments            .00
                                                     Balance Forward      89.16
      Service Period  Days Meter Number             Current  Previous     Usage
 WA  3/13/15  3/26/15  13   99026254                  13066     13057         9
 Service                        Consumption            Charge            Total
 WA  WATER SERVICE                     9.00             15.36            15.36
 SW  SEWER SERVICE                     9.00             16.98            16.98
 GA  SANITATION SERVICE    3/13/15   3/26/15             9.67             9.67
 ST  STORMWATER SERVICE    3/20/15   3/26/15             7.25             7.25
 RE  RECYCLING SERVICE     3/20/15   3/26/15             2.77             2.77
     DEPOSIT REFUND          3/27/15                                     80.00-
     DEPOSIT INTEREST        3/27/15                                       .05-
                             Total Current Charges                       28.02-
                             ** Balance Forward **                       89.16
                             Total Amount Due                            61.14
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ***************************************************************************
     123456789012345678901234567890123456789012345678901234567890123456789012345
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
     ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVW
