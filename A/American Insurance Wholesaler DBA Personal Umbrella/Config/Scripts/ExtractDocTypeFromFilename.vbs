' *******************************************************************
' Extract DocType from Filename
' *******************************************************************
' sOrigFilename Structure (DocType_Unique15CharString_SourceFilename.pdf)
sOrigFilename=watch.getvariable("sOrigFilename")
sDocType=Left(sOrigFilename,InStr(sOrigFilename,"_")-1)
watch.setvariable "sDocType", sDocType
' watch.log sDocType, 1
