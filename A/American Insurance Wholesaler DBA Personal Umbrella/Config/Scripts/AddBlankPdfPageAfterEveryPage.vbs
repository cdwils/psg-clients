'*********************************************************************
' This Script will add a blank pdf page after every page in the document
' so Simplex documents can be sent to print with a global Duplex setting
' http://help.objectiflune.com/files/EN/alambicedit-api/AlambicEdit.html#TPdfRect
' The value of pdfRect.right and pdfRect.top are the size that you want the page x72.
' For example: 11*72 = 792 and 8.5*72 = 612, so these values will give you an 8.5x11 blank page.
'*********************************************************************
Set oPDF = Watch.GetPDFEditObject
oPDF.Open Watch.GetJobFileName, False

set pdfRect = CreateObject("AlambicEdit.PdfRect")
pdfRect.left = 0
pdfRect.top = 792
pdfRect.right = 612
pdfRect.bottom = 0

iCount = oPDF.Pages.count

iArrayIndex = 0

' Since our page index gets reset every time we add a page to the PDF, we need to 
' keep track of the indexes via Array. Then we loop through the array, adding pages
' to the pdf as we go.
For i=1 to iCount
  Redim Preserve aPDFArray(iArrayIndex)
  aPDFArray(iArrayIndex)=i-1
  iArrayIndex = iArrayIndex + 1
Next

if iArrayIndex > 0 then
  For i=0 to uBound(aPDFArray)
    if i=0 then
      oPDF.Pages.insert aPDFArray(i)+1, pdfRect
    else
      oPDF.Pages.insert aPDFArray(i)+i+1, pdfRect
    end if
  'watch.log "aPDFArray: " & aPDFArray(i), 1
  'watch.log "aPDFArraySub: " & aPDFArray(i)+i+1, 1
  Next
end if

oPDF.save True
