 Page 99
             XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
             XXXXXXXXX-XXXXXXXXX XX-XX   99/99/99   99/99/99
                     XXXXXXXXXXXXXXXXXXXXXXXXX  9,999,999.99
                     XXXXXXXXXXXXXXXXXXXXXXXXX  9,999,999.99
                     XXXXXXXXXXXXXXXXXXXXXXXXX  9,999,999.99
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
     XXXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXX
 99999999   XXXXX
                                                              XXXXXXXXXXXXXXXXXX
                  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                                                XXXXXXXXXXXXXXXX  9,999,999.99
 XXXXXXXX-XXXXXXXXXX  XX-XX  99/99/99  99/99/99 XXXXXXXXXXXXXXXX  9,999,999.99
                                                XXXXXXXXXXXXXXXX  9,999,999.99
 XXXXXXXXXXXXX X XXXXXXXXXXXXXXXXXXXX           XXXXXXXXXXXXXXXX  9,999,999.99
           Last payment amount/date: 9,999,999.99  99/99/99
       Service Period Days Meter Number XXXX XXXXX   Current   Previous   Usage
 XX 99/99/99  99/99/99 99  XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 XX 99/99/99  99/99/9999   XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 XX 99/99/99  99/99/9999   XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 XX 99/99/99  99/99/9999   XXXXXXXXXXXX 9999 XXXX 9999999.99999999.99999999.99-
 Service                        Consumption            Charge             Total
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
     XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX      9,999,999.99      9,999,999.99
     XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
 XX  XXXXXXXXXXXXXXXXXXXX                                          9,999,999.99
                             XXXXXXXXXXXXXXXXXXXXXXXXX             9,999,999.99
                             XXXXXXXXXXXXXXXXXXXXXXXXX             9,999,999.99
                             XXXXXXXXXXXXXXXXXXXXXXXXX             9,999,999.99
                           Pay 9,999,999.99 After 99/99/99
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  Page  1
           145 13TH AVE N
           62723-6619          01-09   3/13/15    4/02/15
                  Total Current Charges             20.77
                  ** Balance Forward **               .00
                  Total Amount Due                  20.77
                 Pay         21.81  After    4/07/15
        KATHLEN STERLING
        660 EAST ST                                00006272300000661900000002077
        STAFFORD SPRINGS CT 06076
        1
                  145 13TH AVE N
                                                     Last Bill Amount       .00
  62723-6619          01-09   3/13/15    4/02/15     Payments               .00
                                                     Adjustments            .00
                                                     Balance Forward        .00
 Service                        Consumption            Charge            Total
 SL  STREET LIGHT ASSESS   3/04/15   3/13/15             3.25             3.25
 ST  STORMWATER SERVICE    3/04/15   3/13/15             7.25             7.25
 RE  RECYCLING SERVICE     3/04/15   3/13/15             2.77             2.77
     TURN ON CHARGE          3/05/15                                      7.50
                             Total Current Charges                       20.77
                             ** Balance Forward **                         .00
                             Total Amount Due                            20.77
                             Pay        21.81   After   4/07/15
					TESTING	
					TESTING
		            HTTP://CITYOFSAFETYHARBOR.COM
				   123456789#$%&*
                            TESTING, TESTING, EXTRA LINES
           UTILITY BILLING E-NOTIFICATION AND AUTOMATIC BANK DRAFT AVAILABLE
                       THE SERVICES ARE CONVENIENT AND "GREEN"
            HTTP://CITYOFSAFETYHARBOR.COM/INDEX.ASPX?NID=534 or 727-724-1555
  Page  1
           215 3RD AVE N
           62711-5333          01-06   3/13/15    4/02/15
                  Total Current Charges             24.02
                  ** Balance Forward **               .00
                  Total Amount Due                  24.02
                 Pay         25.22  After    4/07/15
        RICHARD HOWELL
        4101 W EUCLID AVE                          00006271100000533300000002402
        TAMPA FL 33629
        2
                  215 3RD AVE N
                                                     Last Bill Amount       .00
  62711-5333          01-06   3/13/15    4/02/15     Payments               .00
                                                     Adjustments            .00
                                                     Balance Forward        .00
 Service                        Consumption            Charge            Total
 SL  STREET LIGHT ASSESS   3/02/15   3/13/15             6.50             6.50
 ST  STORMWATER SERVICE    3/02/15   3/13/15             7.25             7.25
 RE  RECYCLING SERVICE     3/02/15   3/13/15             2.77             2.77
     TURN ON CHARGE          3/02/15                                      7.50
                             Total Current Charges                       24.02
                             ** Balance Forward **                         .00
                             Total Amount Due                            24.02
                             Pay        25.22   After   4/07/15
                                        TESTING	
					TESTING
		            HTTP://CITYOFSAFETYHARBOR.COM
				   123456789#$%&*
                            TESTING, TESTING, EXTRA LINES
           UTILITY BILLING E-NOTIFICATION AND AUTOMATIC BANK DRAFT AVAILABLE
                       THE SERVICES ARE CONVENIENT AND "GREEN"
            HTTP://CITYOFSAFETYHARBOR.COM/INDEX.ASPX?NID=534 or 727-724-1555
