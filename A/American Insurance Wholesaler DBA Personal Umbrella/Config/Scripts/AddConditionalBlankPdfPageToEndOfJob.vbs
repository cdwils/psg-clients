' This Script will add a blank pdf page at the end of documents if they contain
'  an odd number of pages so documents dont print on the back of other documents
' http://help.objectiflune.com/files/EN/alambicedit-api/AlambicEdit.html#TPdfRect
' The value of pdfRect.right and pdfRect.top are the size that you want the page x72.
' For example: 11*72 = 792 and 8.5*72 = 612, so these values will give you an 8.5x11 blank page.

Set oPDF=Watch.GetPDFEditObject
oPDF.Open Watch.GetJobFileName, true
if oPDF.Pages.Count mod 2 <> 0 then
   set pdfRect = CreateObject("AlambicEdit.PdfRect")
   pdfRect.left = 0
   pdfRect.top = 792
   pdfRect.right = 612
   pdfRect.bottom = 0

   oPDF.Pages.Insert oPDF.Pages.Count, pdfRect
   oPDF.save True
end if
Watch.log "sPageCount: " & oPDF.Pages.Count, 2
oPDF.close
